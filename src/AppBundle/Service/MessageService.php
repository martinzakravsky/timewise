<?php

namespace AppBundle\Service;

use AppBundle\Entity\Message;
use \Doctrine\Common\Collections\ArrayCollection as ArrayCollection;
use Doctrine\ORM\PersistentCollection;
use AppBundle\Entity\Shift;
use Doctrine\ORM\EntityManager as EntityManager;

class MessageService
{
    private $em;

    public function __construct(EntityManager $em) {
        $this -> em = $em;
    }

    public function shiftChangeNotify($shift, $originator, $action) {
        $message = new Message();
        $explanation1 = "";
        $explanation2 = "";
        switch ($action) {
            case Shift::CONFIRMED:
                $explanation1 = "has confirmed";
                break;
            case Shift::CANCELLED:
                $explanation1 = "has cancelled";
                break;
            case Shift::REJECTED:
                $explanation1 = "has rejected";
                break;
            case Shift::FINISHED:
                $explanation1 = "has marked";
                $explanation2 = "as finished";
                break;
            case Shift::MISSED:
                $explanation1 = "has marked";
                $explanation2 = "as missed";
                break;
        }

        $message -> setContent($originator -> getName() . " " . $originator -> getSurname() . " (" . $originator -> getUsername() . ") " . $explanation1 . " your shift (" . $shift -> getTimeslot() -> getDate() -> format("d. m. Y") . ", as " . $shift -> getJob() -> getName() . ") " . $explanation2 . ".");
        $message -> setUser($shift -> getUser());

        $this -> em -> persist($message);
        $this -> em -> flush();
    }

}