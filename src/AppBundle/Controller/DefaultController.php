<?php

namespace AppBundle\Controller;

use Doctrine\Common\Collections\ArrayCollection;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use AppBundle\Entity\Job;
use AppBundle\Entity\User;

class DefaultController extends Controller
{
    /**
     * @Route("/", name="dashboard")
     */
    public function indexAction(Request $request)
    {

        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR
        ]);
    }

    /**
     * @Route("/translate/", name="translate")
     * @Method({"GET","POST"})
     *
     * Route for JavaScript translations
     */
    public function translateAction(Request $request) {
        $data = $request -> request -> get('original');

        $res_data = $this -> get('translator') -> trans($data);

        if (empty($res_data)) $res_data = $data;

        return new JsonResponse(['translated' => $res_data]);

    }

    // TODO DELETE

    /**
     * @Route("/test", name="test")
     */
    public function testAction(Request $request) {
        return $this -> render("default/test.html.php");
    }
}
