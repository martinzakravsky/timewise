<?php

namespace AppBundle\Controller;

use AppBundle\Form\UserWageType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

class AccountantController extends Controller
{
    /**
     * @Route("/accountant/")
     * @Route("/accountant/{page}", name="accountant")
     */
    public function userOverviewAction($page = 1)
    {
        $this->denyAccessUnlessGranted('ROLE_ACCOUNTANT', NULL, 'error.accessDenied');

        $paginationWidget = $this -> get('pagination_widget');

        $users = $this -> getDoctrine() -> getRepository("AppBundle:User") -> findAll();

        $paginationWidget -> setItems($users);
        $paginationWidget -> setCurrentPage($page);

        $forms = array();

        foreach($users as $user) {
            $forms[] = $this -> createForm(UserWageType::class, $user, [
                'action' => $this -> generateUrl('accountant_modify_user_wage', [
                    'id' => $user -> getId(),
                ]),
                'method' => 'POST',
            ]) -> createView();
        }

        return $this->render('accountant/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'users' => $paginationWidget -> getCurrentPageData(),
            'forms' => $forms,
            'pw' => $paginationWidget,
        ]);
    }


}
