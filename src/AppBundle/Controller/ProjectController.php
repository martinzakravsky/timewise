<?php

namespace AppBundle\Controller;
use AppBundle\Exception\InvalidInputException;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\RedirectResponse;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Request;
use AppBundle\Entity\User;
use AppBundle\Entity\Project;
use AppBundle\Form\UserGeneralType;
use AppBundle\Form\UserProjectType;
use AppBundle\Exception\UserNotFoundException;
use AppBundle\Form\ProjectType;

class ProjectController extends Controller
{
    /**
     * @Route("/switch_project/{projectID}", name="switch_project")
     */
    public function switchAction(Request $request, $projectID)
    {
        if (!is_numeric($projectID) || $projectID < 0) {
            throw $this -> createNotFoundException();
        }

        // Musíme aktualizovat uživatele v databázi, nikoliv v session

        $userRepo = $this -> getDoctrine() -> getRepository("AppBundle:User");
        $user = $userRepo -> findOneById($this -> getUser() -> getID());

        $projectRepo = $this -> getDoctrine() -> getRepository("AppBundle:Project");
        if ($projectID != 0) {
            $project = $projectRepo->findOneById($projectID);
        }
        else {
            $project = null;
        }

        if (!($user -> getAvailableProjects() -> contains($project)) && $project != null) {
            throw $this -> createNotFoundException();
        }

        $user -> setPreferredProject($project);

        $fosUM = $this -> get('fos_user.user_manager');
        $fosUM -> updateUser($user);

        $em = $this -> getDoctrine() -> getManager();
        $em -> persist($user);
        $em -> flush($user);

        return $this -> redirectToRoute('dashboard');
    }

    /**
     * @Route("/admin/create_project", name="admin_create_project")
     */
    public function createAction(Request $request)
    {
        // return $this -> modifyAction($request, 0);
        return $this -> redirectToRoute('admin_modify_project', [
            'request' => $request,
            'id' => 0,
        ]);
    }

    /**
     * @Route("/admin/modify_project/{id}", name="admin_modify_project")
     */
    public function modifyAction(Request $request, $id) {
        $this -> denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'You don\'t have the necessary permissions to access this function');

        if (!is_numeric($id) || $id < 0) throw $this -> createNotFoundException();

        if ($id == 0) {
            $project = new Project();
        }
        else {
            $repo = $this->getDoctrine()->getRepository("AppBundle:Project");
            $project = $repo->findOneById($id);
            if ($project == null) throw $this -> createNotFoundException();
        }

        $fosUM = $this -> get('fos_user.user_manager');

        $form = $this -> createForm(ProjectType::class, $project);

        $originalJobs = new ArrayCollection();
        foreach ($project -> getJobs() as $job) {
            $originalJobs -> add($job);
        }

        $form -> handleRequest($request);

        if ($form -> isSubmitted()) {
            if ($form -> isValid()) {
                $em = $this -> getDoctrine() -> getManager();
                try {
                    foreach ($originalJobs as $job) {
                        if (!($project -> getJobs() -> contains($job))) {
                            $project -> removeJob($job);
                            $em -> remove($job);
                        }
                    }

                    $em->persist($project);
                    $em->flush();
                    $this -> addFlash("notice", "Project saved!");
                }
                catch(UniqueConstraintViolationException $ex) {
                    $this -> addFlash("error", "Each project must have a unique name");
                    return $this -> redirect($request -> headers -> get('referer'));
                }
                catch(\Doctrine\DBAL\DBALException $ex) {
                    $this -> addFlash('error', "The job you are trying to delete is active and in use");
                }
                catch(\Exception $e) {
                    $this -> addFlash("error", "Unknown error occurred");
                }

                return $this -> redirectToRoute("admin_project_overview");
            }
        }




        return $this->render('admin/modify_project.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.root_dir').'/..').DIRECTORY_SEPARATOR,
            'form' => $form -> createView(),
            'project' => $project,
        ]);

    }

    /**
     * @Route("/admin/delete_project/{id}", name="admin_delete_project")
     */
    public function deleteAction(Request $request, $id) {
        $this -> denyAccessUnlessGranted('ROLE_ADMIN', NULL, 'You don\'t have the necessary permissions to access this function');

        $em = $this -> getDoctrine() -> getManager();

        $projectRepo = $this -> getDoctrine() -> getRepository("AppBundle:Project");

        $project = $projectRepo -> findOneById($id);

        if ($project == null || !($this -> isCsrfTokenValid('delete_'.$project -> getId(), $request -> get('_csrf_token')))) {
            throw new InvalidInputException("CSRF protection violated");
        }

        try {
            $em->remove($project);
            $em->flush();
            $this -> addFlash('notice', 'Project deleted!');
        }
        catch(\Doctrine\DBAL\DBALException $ex) {
            $this -> addFlash('error', "The project you are trying to delete has active jobs");
        }
        catch(\Exception $ex) {
            $this -> addFlash('error', 'Unknown error occurred!');
        }

        return $this -> redirect($request -> headers -> get('referer'));
    }
 }
