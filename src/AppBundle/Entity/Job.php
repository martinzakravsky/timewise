<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\JobRepository")
 * @ORM\Table(name="jobs")
 */
class Job
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=false)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="text", nullable=true)
     */
    private $comment;

    /**
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="jobs", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private $project;


    /**
     * @ORM\OneToMany(targetEntity="Timeslot", mappedBy="job")
     */
    private $timeslots;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="supervising")
     * @ORM\JoinTable(name="users_jobs_s")
     */
    private $supervisors;

    /**
     * @ORM\ManyToMany(targetEntity="User", mappedBy="attending")
     * @ORM\JoinTable(name="users_jobs_e")
     */
    private $employees;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    private $version;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Job
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return Job
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set roles
     *
     * @param \AppBundle\Entity\User $roles
     *
     * @return Job
     */
    public function setJobs(\AppBundle\Entity\User $jobs = null)
    {
        $this->jobs = $jobs;

        return $this;
    }

    /**
     * Get roles
     *
     * @return \AppBundle\Entity\User
     */
    public function getRoles()
    {
        return $this->roles;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this -> supervisors = new \Doctrine\Common\Collections\ArrayCollection();
        $this -> employees = new ArrayCollection();
        $this -> timeslots = new ArrayCollection();
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return Job
     */
    public function setProject(\AppBundle\Entity\Project $project = null)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\User
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return Job
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }

    /**
     * Set supervisors
     *
     * @param \AppBundle\Entity\User $supervisors
     *
     * @return Job
     */
    public function setSupervisors(\AppBundle\Entity\User $supervisors = null)
    {
        $this->supervisors = $supervisors;

        return $this;
    }

    /**
     * Get supervisors
     *
     * @return \AppBundle\Entity\User
     */
    public function getSupervisors()
    {
        return $this->supervisors;
    }

    /**
     * Add timeslot
     *
     * @param \AppBundle\Entity\Timeslot $timeslot
     *
     * @return Job
     */
    public function addTimeslot(\AppBundle\Entity\Timeslot $timeslot)
    {
        $this->timeslots[] = $timeslot;

        return $this;
    }

    /**
     * Remove timeslot
     *
     * @param \AppBundle\Entity\Timeslot $timeslot
     */
    public function removeTimeslot(\AppBundle\Entity\Timeslot $timeslot)
    {
        $this->timeslots->removeElement($timeslot);
    }

    /**
     * Get timeslots
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTimeslots()
    {
        return $this->timeslots;
    }

    /**
     * Add supervisor
     *
     * @param \AppBundle\Entity\User $supervisor
     *
     * @return Job
     */
    public function addSupervisor(\AppBundle\Entity\User $supervisor)
    {
        $this->supervisors[] = $supervisor;

        return $this;
    }

    /**
     * Remove supervisor
     *
     * @param \AppBundle\Entity\User $supervisor
     */
    public function removeSupervisor(\AppBundle\Entity\User $supervisor)
    {
        $this->supervisors->removeElement($supervisor);
    }

    /**
     * Add employee
     *
     * @param \AppBundle\Entity\User $employee
     *
     * @return Job
     */
    public function addEmployee(\AppBundle\Entity\User $employee)
    {
        $this->employees[] = $employee;

        return $this;
    }

    /**
     * Remove employee
     *
     * @param \AppBundle\Entity\User $employee
     */
    public function removeEmployee(\AppBundle\Entity\User $employee)
    {
        $this->employees->removeElement($employee);
    }

    /**
     * Get employees
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEmployees()
    {
        return $this->employees;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return Job
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }
}
