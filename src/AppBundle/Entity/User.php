<?php

namespace AppBundle\Entity;

use AppBundle\Exception\InvalidInputException;
use Doctrine\ORM\Mapping as ORM;
use FOS\UserBundle\Model\User as BaseUser;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\ArrayCollection;
use AppBundle\Entity\Project as Project;
use AppBundle\Entity\User as User;
use AppBundle\Entity\Shift;
use Symfony\Component\Security\Core\User\EquatableInterface as EquatableInterface;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(name="users",uniqueConstraints={@ORM\UniqueConstraint(name="username", columns={"username"}), @ORM\UniqueConstraint(name="email", columns={"email"})})
 */
class User extends BaseUser
{
    /**
     * @var integer $id
     *
     * @ORM\Column(name="id", type="bigint")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=255, nullable=false)
     */
    // protected $username;

    /**
     * @var string $email
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false)
     * @Assert\Email(
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     */
    // protected $email;

    /**
     * @var string $name
     *
     * @ORM\Column(name="name", type="string", length=255, nullable=true)
     */
    private $name = '';

    /**
     * @var string
     *
     * @ORM\Column(name="surname", type="string", length=255, nullable=true)
     */
    private $surname = '';

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_of_birth", type="date", nullable=true)
     */
    private $dateOfBirth = null;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private $comment = '';

    /**
     * @ORM\ManyToMany(targetEntity="Job", inversedBy="employees")
     * @ORM\JoinTable(name="users_jobs_e")
     * @ORM\OrderBy({"name"="ASC"})
     */
    private $attending;

    /**
     * @ORM\ManyToMany(targetEntity="Job", inversedBy="supervisors")
     * @ORM\JoinTable(name="users_jobs_s")
     * @ORM\OrderBy({"name"="ASC"})
     */
    private $supervising;

    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="Project")
     * @ORM\JoinColumn(name="preferred_project_id", referencedColumnName="id", nullable=true)
     */
    private $preferredProject = null;


    /**
     *
     * @ORM\OneToMany(targetEntity="Shift", mappedBy="user")
     */
    private $shifts;

    /**
     * Messages
     *
     * @var Message
     * @ORM\OneToMany(targetEntity="Message", mappedBy="user")
     * @ORM\OrderBy({"datetimeOfOrigin" = "DESC"})
     */
    private $messages;

    /**
     * @ORM\Column(type="integer")
     * @ORM\Version
     */
    private $version;

    /**
     * Wage
     *
     * @ORM\Column(type="decimal", nullable=true)
     */
    private $wage = 0;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set username
     *
     * @param string $username
     *
     * @return User
     */
    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username
     *
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return User
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set surname
     *
     * @param string $surname
     *
     * @return User
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;

        return $this;
    }

    /**
     * Get surname
     *
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * Set dateOfBirth
     *
     * @param \DateTime $dateOfBirth
     *
     * @return User
     */
    public function setDateOfBirth($dateOfBirth)
    {
        $this->dateOfBirth = $dateOfBirth;

        return $this;
    }

    /**
     * Get dateOfBirth
     *
     * @return \DateTime
     */
    public function getDateOfBirth()
    {
        return $this->dateOfBirth;
    }


    /**
     * Set comment
     *
     * @param string $comment
     *
     * @return User
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }


    /**
     * Constructor
     */
    public function __construct()
    {
        parent :: __construct();
        $this -> attending = new ArrayCollection();
        $this -> supervising = new ArrayCollection();
        $this -> messages = new ArrayCollection();
        $this -> shifts = new ArrayCollection();
    }

    /**
     * Add job
     *
     * @param \AppBundle\Entity\Job $job
     *
     * @return User
     */
    public function addAttending(\AppBundle\Entity\Job $job)
    {
        $this->attending[] = $job;

        return $this;
    }

    /**
     * Remove job
     *
     * @param \AppBundle\Entity\Job $job
     */
    public function removeAttending(\AppBundle\Entity\Job $job)
    {
        $this->attending->removeElement($job);
    }

    /**
     * Get attending
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttending()
    {
        return $this->attending;
    }

    /**
     * Add supervising
     *
     * @param \AppBundle\Entity\Job $supervising
     *
     * @return User
     */
    public function addSupervising(\AppBundle\Entity\Job $supervising)
    {
        $this->supervising[] = $supervising;

        return $this;
    }

    /**
     * Remove supervising
     *
     * @param \AppBundle\Entity\Job $supervising
     */
    public function removeSupervising(\AppBundle\Entity\Job $supervising)
    {
        $this->supervising->removeElement($supervising);
    }

    /**
     * Get supervising
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervising()
    {
        return $this->supervising;
    }

    /**
     * Get all projects that the user is attending
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAttendingProjects() {
        $res = new ArrayCollection();

        foreach ($this -> attending as $job) {
            $project = ($job -> getProject());
            if ($project != null && !($res -> contains($project))) {
                $res -> add($project);
            }
        }

        return $res;
    }

    /**
     * Get all projects that the user is supervising
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSupervisingProjects() {
        $res = new ArrayCollection();

        foreach ($this -> supervising as $job) {
            $project = ($job -> getProject());
            if ($project != null && !($res -> contains($project))) {
                $res -> add($project);
            }
        }

        return $res;
    }

    /**
     * Get all projects available for the user - supervising or attending
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAvailableProjects() {
        $res = new ArrayCollection();
        $attending = $this -> getAttendingProjects();
        $supervising = $this -> getSupervisingProjects();

        foreach ($supervising as $project) {
            if (!($res -> contains($project))) {
                $res -> add($project);
            }
        }

        foreach ($attending as $project) {
            if (!($res -> contains($project))) {
                $res -> add($project);
            }
        }



        return $res;
    }

    /**
     * Set preferredProject
     *
     * @param \AppBundle\Entity\Project $preferredProject
     *
     * @return User
     */
    public function setPreferredProject(\AppBundle\Entity\Project $preferredProject = null)
    {
        $this->preferredProject = $preferredProject;

        return $this;
    }

    /**
     * Get preferredProject
     *
     * @return \AppBundle\Entity\Project
     */
    public function getPreferredProject()
    {
        return $this->preferredProject;
    }


    /**
     * Add shift
     *
     * @param \AppBundle\Entity\Shift $shift
     *
     * @return User
     */
    public function addShift(\AppBundle\Entity\Shift $shift)
    {
        $this->shifts[] = $shift;

        return $this;
    }

    /**
     * Remove shift
     *
     * @param \AppBundle\Entity\Shift $shift
     */
    public function removeShift(\AppBundle\Entity\Shift $shift)
    {
        $this->shifts->removeElement($shift);
    }

    /**
     * Get shifts
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShifts()
    {
        return $this->shifts;
    }



    /**
     * @param Timeslot $timeslot
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShiftsByTimeslot(Timeslot $timeslot) {
        $res = $this -> getShifts() -> filter(function($shift) use ($timeslot){
            return $shift -> getTimeslot() == $timeslot;
        });

        return $res;
    }

    /**
     * Set version
     *
     * @param integer $version
     *
     * @return User
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Add message
     *
     * @param \AppBundle\Entity\Message $message
     *
     * @return User
     */
    public function addMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages[] = $message;

        return $this;
    }

    /**
     * Remove message
     *
     * @param \AppBundle\Entity\Message $message
     */
    public function removeMessage(\AppBundle\Entity\Message $message)
    {
        $this->messages->removeElement($message);
    }

    /**
     * Get messages
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMessages()
    {
        return $this->messages;
    }

    /**
     * Set wage
     *
     * @param $wage
     *
     * @return User
     */
    public function setWage($wage)
    {
        if ($wage < 0) throw new InvalidInputException();

        $this->wage = $wage;

        return $this;
    }

    /**
     * Get wage
     */
    public function getWage()
    {
        return $this->wage;
    }
}
