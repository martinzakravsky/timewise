<?php

namespace AppBundle\Form;

use AppBundle\Entity\User;
use AppBundle\Entity\Project;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;


class UserWageType extends AbstractType {
    public function buildForm(FormBuilderInterface $builder, array $options) {
        $builder
            ->add('wage', MoneyType::class, [
                'label' => false,
                'currency' => '',
                'attr' => [
                    'class' => 'money_field'
                ]
            ])
            ->add('submit', SubmitType::class, [
                'label_format' => 'OK',
                'attr' => [
                    'class' =>  'money_submit'
                ]
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver) {
        $resolver->setDefaults(array(
            'data_class' => User::class,
        ));
    }
}