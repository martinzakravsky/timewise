<?php
    class Foo {
        public function speak() {
            echo "Hello world!";
        }
    }

    class Bar {
        public function callSpeak(Foo $f) {
            $f -> speak();
        }
    }

    $foo = new Foo();
    $bar = new Bar();
    $bar2 = new Bar();

    $bar -> callSpeak($bar2);
?>