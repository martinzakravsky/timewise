INSERT INTO projects (name, description, allows_overlapping_shifts, comment) VALUES
  (
    'SuperObchod s.r.o.',
    'Samoobslužná drogérie na Pankráci',
    '0',
    'TEST DATA'
  ),
  (
    'Best cars a.s.',
    'Firma zabývající se výrobou automobilů',
    '0',
    'TEST DATA'
  ),
  (
    'Mega Company',
    'Firma zabývající se vším všudy',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 1',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 2',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 3',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 4',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 5',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 6',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 7',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 8',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 9',
    'Description of dummy project',
    '0',
    'TEST DATA'
  ),
  (
    'Dummy project 10',
    'Description of dummy project',
    '0',
    'TEST DATA'
  )
;


INSERT INTO jobs (project_id, name, comment) VALUES
  (
    '1',
    'Cashier',
    'TEST DATA'
  ),
  (
    '1',
    'Manager',
    'TEST DATA'
  ),
  (
    '1',
    'Janitor',
    'TEST DATA'
  ),
  (
    '2',
    'Manufacturer',
    'TEST DATA'
  )
;


INSERT INTO users_jobs_e (user_id, job_id) VALUES
  ('1', '1'),
  ('1', '2'),
  ('1', '3'),
  ('2', '1'),
  ('2', '2'),
  ('3', '3')
  ;

INSERT INTO users_jobs_s (user_id, job_id) VALUES
  ('1', '1'),
  ('1', '2')
  ;

UPDATE users SET
    name = 'John',
    surname = 'Doe'
WHERE id = 1;

UPDATE users SET
    name = 'Anne',
    surname = 'Smith'
WHERE id = 2;

UPDATE users SET
    name = 'Luke',
    surname = 'Anderson'
WHERE id = 3;

INSERT INTO timeslots (id, job_id, date, begin_time, end_time, capacity, open, comment) VALUES
  (NULL, '1', '2017-04-08', '00:00:00', '01:00:00', '2', '1', 'TEST DATA'),
  (NULL, '2', '2017-04-08', '00:00:00', '00:02:00', '2', '1', 'TEST DATA'),
  (NULL, '3', '2017-04-08', '00:00:00', '00:03:00', '2', '1', 'TEST DATA'),
  (NULL, '4', '2017-04-08', '00:00:00', '00:04:00', '2', '1', 'TEST DATA'),
  (NULL, '1', '2017-04-09', '00:00:00', '00:05:00', '2', '0', 'TEST DATA'),
  (NULL, '1', '2017-04-10', '00:00:00', '00:06:00', '2', '1', 'TEST DATA'),
  (NULL, '3', '2017-04-11', '00:00:00', '00:07:00', '2', '1', 'TEST DATA')
  ;

INSERT INTO shifts (id, timeslot_id, user_id, state, comment) VALUES
  (NULL, '1', '2', '0', NULL),
  (NULL, '2', '2', '0', NULL),
  (NULL, '5', '2', '0', NULL),
  (NULL, '6', '2', '0', NULL),
  (NULL, '3', '3', '0', NULL),
  (NULL, '7', '3', '0', NULL)
  ;
